provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket         = "eddies-ecs-fargate-terraform"
    key            = "recipe-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}